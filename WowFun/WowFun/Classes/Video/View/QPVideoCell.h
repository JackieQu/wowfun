//
//  QPVideoCell.h
//  WowFun
//
//  Created by JackieQu on 2019/8/19.
//  Copyright © 2019 WowFun. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface QPVideoCell : UICollectionViewCell

@end

NS_ASSUME_NONNULL_END
