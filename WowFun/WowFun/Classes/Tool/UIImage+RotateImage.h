//
//  UIImage+RotateImage.h
//  WowFun
//
//  Created by JackieQu on 2019/8/14.
//  Copyright © 2019 WowFun. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (RotateImage)

- (UIImage *)rotateImageWithRadian:(CGFloat)radian;

@end

NS_ASSUME_NONNULL_END
